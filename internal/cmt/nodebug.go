// +build !debug

package cmt

type noopPrinter struct{}

func (n *noopPrinter) Print(...interface{})          {}
func (n *noopPrinter) Printf(string, ...interface{}) {}

var Debug noopPrinter
