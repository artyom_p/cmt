package cmt

// Msg is a message to be pushed to comet client
type Msg interface{}

// Payload holds one message and its "offset" as timestamp of message submission
type Payload struct {
	Msg    Msg
	Offset int64
}

// printer is used for debug output
type printer interface {
	Print(...interface{})
	Printf(string, ...interface{})
}
