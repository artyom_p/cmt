// +build debug

package cmt

import (
	"log"
	"os"
)

var Debug printer = log.New(os.Stderr, "DEBUG ", log.Lshortfile)
