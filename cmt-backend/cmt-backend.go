package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"log"
	"net/http"
	"time"

	"bitbucket.org/artyom_p/cmt/internal/cmt"
	"github.com/artyom/autoflags"
	"github.com/fzzy/radix/redis"
)

func main() {
	config := struct {
		RedisAddr string `flag:"redis,redis tcp address (host:port)"`
		Channel   string `flag:"pub,channel to publish to"`
		Bind      string `flag:"l,address to listen at (host:port)"`
	}{
		RedisAddr: "localhost:6379",
		Channel:   "_comet_broadcast",
		Bind:      "localhost:8081",
	}
	if err := autoflags.Define(&config); err != nil {
		log.Fatal(err)
	}
	flag.Parse()
	b := NewBackend(config.Channel, config.RedisAddr)
	go b.Wait()
	server := &http.Server{
		Addr:         config.Bind,
		Handler:      b,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
	}
	log.Fatal(server.ListenAndServe())
}

func NewBackend(pubName, redisAddr string) *Backend {
	return &Backend{
		dataChan:    make(chan queueItem, 100),
		channelName: pubName,
		redisAddr:   redisAddr,
	}
}

type Backend struct {
	dataChan chan queueItem

	channelName string
	redisAddr   string
}

func (b *Backend) Wait() {
	retryDelay := 5 * time.Second
CONNLOOP:
	for {
		conn, err := redis.Dial("tcp", b.redisAddr)
		if err != nil {
			log.Printf("failed to connect to redis: %s, would retry in %s", err, retryDelay)
			time.Sleep(retryDelay)
			continue CONNLOOP
		}
		for item := range b.dataChan {
			buf := new(bytes.Buffer)
			if err := json.NewEncoder(buf).Encode(item.payload); err != nil {
				log.Print("failed to marshal payload:", err)
				continue
			}
			conn.Append("RPUSH", item.queue, buf.Bytes())
			conn.Append("LTRIM", item.queue, -30, -1) // FIXME make threshold configurable
			conn.Append("PUBLISH", b.channelName, item.queue)
		READREPLY:
			for {
				r := conn.GetReply()
				switch r.Err {
				case nil:
				case redis.PipelineQueueEmptyError:
					break READREPLY
				default:
					log.Print("redis session error:", r.Err)
					conn.Close()
					continue CONNLOOP
				}
			}
		}
	}
}

func (b *Backend) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "POST":
	default:
		http.Error(w, "unsupported method", http.StatusMethodNotAllowed)
		return
	}
	chId := r.URL.Query().Get("channel")
	if len(chId) == 0 {
		http.Error(w, "channel not specified", http.StatusBadRequest)
		return
	}
	// read r.Body, parse it as cmt.Msg, create cmt.Payload from it, adding
	// Offset to Msg as time.Now().UnixNano(), put payload to redis queue,
	// notify subscribers by publishing queue name
	var msg interface{}
	if err := json.NewDecoder(r.Body).Decode(&msg); err != nil {
		http.Error(w, "invalid message format", http.StatusBadRequest)
		return
	}
	item := queueItem{
		queue: chId,
		payload: &cmt.Payload{
			Msg:    msg,
			Offset: time.Now().UnixNano(),
		},
	}
	select {
	case b.dataChan <- item:
	default:
		http.Error(w, "local queue is already full", http.StatusInternalServerError)
		return
	}
}

type queueItem struct {
	queue   string
	payload *cmt.Payload
}
