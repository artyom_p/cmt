package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
	"sync"
	"time"

	"bitbucket.org/artyom_p/cmt/internal/cmt"

	"github.com/artyom/autoflags"
	"github.com/fzzy/radix/extra/pool"
	"github.com/fzzy/radix/extra/pubsub"
)

func main() {
	config := struct {
		RedisAddr string `flag:"redis,redis tcp address (host:port)"`
		Channel   string `flag:"sub,channel to subscribe to"`
		Bind      string `flag:"l,address to listen at (host:port)"`
	}{
		RedisAddr: "localhost:6379",
		Channel:   "_comet_broadcast",
		Bind:      "localhost:8080",
	}
	if err := autoflags.Define(&config); err != nil {
		log.Fatal(err)
	}
	flag.Parse()
	f := NewFrontend(config.Channel, config.RedisAddr)
	go f.Wait()
	server := &http.Server{
		Addr:         config.Bind,
		Handler:      f,
		ReadTimeout:  1 * time.Minute,
		WriteTimeout: 1 * time.Minute,
	}
	log.Fatal(server.ListenAndServe())
}

func NewFrontend(subName, redisAddr string) *Frontend {
	return &Frontend{
		receivers:   make(map[string][]*Receiver),
		channelName: subName,
		pool:        pool.NewOrEmptyPool("tcp", redisAddr, 10),
	}
}

type Frontend struct {
	m           sync.RWMutex
	receivers   map[string][]*Receiver
	channelName string

	pool *pool.Pool
}

// Wait listens on subscribed channel and calls drainQueue for matched local
// readers. This function never returns, run it in a separate goroutine.
func (f *Frontend) Wait() {
	retryDelay := 5 * time.Second
CONNLOOP:
	for {
		conn, err := f.pool.Get()
		if err != nil {
			log.Printf("failed to connect to redis: %s, would retry in %s", err, retryDelay)
			time.Sleep(retryDelay)
			continue CONNLOOP
		}
		sc := pubsub.NewSubClient(conn)
		if sr := sc.Subscribe(f.channelName); sr.Err != nil {
			log.Printf("failed to subscribe to channel %q: %s, would retry in %s",
				f.channelName, sr.Err, retryDelay)
			f.pool.Put(conn)
			time.Sleep(retryDelay)
			continue CONNLOOP
		}
		for {
			r := sc.Receive()
			switch {
			case r.Err == nil:
			case r.Timeout():
				log.Print("timed out receiving message from channel:", r.Err)
				continue
			default:
				log.Print("error receiving message from channel:", r.Err)
				conn.Close()
				continue CONNLOOP
			}
			rcv := f.readers(r.Message)
			if len(rcv) == 0 {
				continue
			}
			go f.drainQueue(r.Message, rcv)
		}
	}
}

// drainQueue reads given queue payload from redis, filters payload read by each
// receiver's offset (whether or not payload should be delivered to receiver)
// then tries to send reply to receiver.
func (f *Frontend) drainQueue(qName string, receivers []*Receiver) {
	cmt.Debug.Printf("drainQueue called: %q, %s", qName, receivers)
	defer cmt.Debug.Printf("drainQueue for %q returned", qName)
	if len(receivers) == 0 {
		return
	}
	connReleased := false
	conn, err := f.pool.Get()
	if err != nil {
		log.Print("failed to connect to redis:", err)
		return
	}
	defer func() {
		if !connReleased {
			f.pool.Put(conn)
		}
	}()
	r := conn.Cmd("LRANGE", qName, 0, -1)
	if r.Err != nil {
		log.Print("failed to get queue elements:", err)
		return
	}
	f.pool.Put(conn) // XXX early release, do NOT use conn below
	connReleased = true
	cmt.Debug.Printf("got %d elements from queue", len(r.Elems))
	qpl := make([]*cmt.Payload, 0, len(r.Elems))
	for _, item := range r.Elems {
		buf, err := item.Bytes()
		if err != nil {
			continue
		}
		p := new(cmt.Payload)
		if err := json.Unmarshal(buf, p); err != nil {
			cmt.Debug.Print("failed to unmarshal payload:", err)
			continue
		}
		qpl = append(qpl, p)
	}
	if len(qpl) == 0 {
		return
	}
	for _, rcv := range receivers {
		rcv.Lock()
		rpl := make([]*cmt.Payload, 0, len(qpl))
		for _, p := range qpl {
			if p.Offset <= rcv.offset {
				continue
			}
			rpl = append(rpl, p)
		}
		if len(rpl) == 0 {
			rcv.Unlock()
			continue
		}
		go func(rcv *Receiver) {
			defer rcv.Unlock()
			if err := writePayloadToReceiver(rcv, rpl); err != nil {
				log.Print("error writing reply:", err) // XXX would be too noisy
				return
			}
			if f, ok := rcv.Writer.(http.Flusher); ok {
				f.Flush()
			}
			for _, p := range rpl {
				if p.Offset > rcv.offset {
					rcv.offset = p.Offset
				}
			}
		}(rcv)
	}
}

// writePayloadToReceiver marshals payload data to valid ClientReply and writes
// it to underlying Receiver's io.Writer
func writePayloadToReceiver(rcv *Receiver, pl []*cmt.Payload) error {
	max := rcv.offset
	msg := make([]cmt.Msg, len(pl))
	for i, p := range pl {
		msg[i] = p.Msg
		if p.Offset > max {
			max = p.Offset
		}
	}
	reply := ClientReply{Offset: max, Data: msg}
	return json.NewEncoder(rcv.Writer).Encode(reply)
}

// ServeHTTP implements http.Handler interface
func (f *Frontend) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET", "HEAD":
	default:
		http.Error(w, "unsupported method", http.StatusMethodNotAllowed)
		return
	}
	chId := r.URL.Query().Get("channel")
	if len(chId) == 0 {
		http.Error(w, "channel not specified", http.StatusBadRequest)
		return
	}
	offset, err := strconv.ParseInt(r.URL.Query().Get("offset"), 10, 64)
	if err != nil {
		offset = -1
	}
	w.Header().Set("Content-Type", "application/x-javascript; charset=utf-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	rcv := &Receiver{Writer: w, offset: offset}
	f.register(chId, rcv)
	defer f.deregister(chId, rcv)
	timer := time.NewTimer(30 * time.Second)
	defer timer.Stop()
	go f.drainQueue(chId, []*Receiver{rcv}) // just in case if we have non-empty queue
	<-timer.C
	// lock to make sure we don't break any background write operations
	// already in progress
	rcv.Lock()
	// unlock so background writes that managed to get rcv AFTER the Lock()
	// above but BEFORE f.deregister() call won't block forever waiting on
	// lock
	rcv.Unlock()
}

func (f *Frontend) register(id string, r *Receiver) {
	if id == "" || r == nil {
		panic(fmt.Sprintf("invalid id (%s) of receiver (%v)", id, r))
	}
	f.m.Lock()
	defer f.m.Unlock()
	f.receivers[id] = append(f.receivers[id], r)
}

func (f *Frontend) readers(id string) []*Receiver {
	f.m.RLock()
	defer f.m.RUnlock()
	if len(f.receivers[id]) == 0 {
		return nil
	}
	out := make([]*Receiver, len(f.receivers[id]))
	copy(out, f.receivers[id])
	return out
}

func (f *Frontend) deregister(id string, r *Receiver) {
	if id == "" || r == nil {
		panic(fmt.Sprintf("invalid id (%s) of receiver (%v)", id, r))
	}
	f.m.Lock()
	defer f.m.Unlock()
	if len(f.receivers[id]) <= 1 {
		delete(f.receivers, id) // shortcut for most cases
		return
	}
	idx := -1
	s := f.receivers[id]
	for i, v := range s {
		if v == r {
			idx = i
			break
		}
	}
	if idx < 0 {
		return
	}
	// remove item from slice cleaning pointer from underlying array so it
	// can be garbage collected
	copy(s[idx:], s[idx+1:])
	s[len(s)-1] = nil
	s = s[:len(s)-1]
	f.receivers[id] = s
}

type Receiver struct {
	sync.Mutex
	io.Writer
	offset int64
}

type ClientReply struct {
	Offset int64     `json:"new_offset"`
	Data   []cmt.Msg `json:"data"`
}
